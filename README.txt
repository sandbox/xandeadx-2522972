Example:

/**
 * Implements hook_menu().
 */
function mymodule_payment_menu() {
  $items['my-payment-form'] = array(
    'title' => 'Payment form',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mymodule_payment_form'),
    'access arguments' => array('access content'),
  );
  return $items;
}

/**
 * Payment form.
 */
function mymodule_payment_form($form, &$form_state, $submission_id) {
  $form = webpayby_api_payment_form($form, $form_state, array(
    'order_num' => 123,
    'currency_id' => 'BYR',
    'invoice_item_name[0]' => 'Super puper item',
    'invoice_item_quantity[0]' => 1,
    'invoice_item_price[0]' => 7000,
    'total' => 7000,
    'email' => 'customer@gmail.com',
    'phone' => '123123123',
  ));
  return $form;
}

/**
 * Implements hook_webpayby_api_notify().
 */
function needacar_payment_webpayby_api_notify($order_id, $transaction) {
  $order = node_load($order_id);
  $order_id->field_payment_status['und'][0]['value'] = 'complete';
  node_save($order);
}

/**
 * Implements hook_webpayby_api_complete().
 */
function needacar_payment_webpayby_api_complete($order_id, $transaction) {
  return t('Payment complete!');
}
