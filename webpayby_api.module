<?php

/**
 * Implements hook_menu().
 */
function webpayby_api_menu() {
  $items = array();

  $items['admin/config/system/webpayby'] = array(
    'title' => 'Webpay.by',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webpayby_api_settings_form'),
    'access arguments' => array('administer site configuration'),
  );

  $items['webpayby/notify'] = array(
    'page callback' => 'webpayby_api_notify',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  $items['webpayby/complete'] = array(
    'title' => 'Payment is completed',
    'page callback' => 'webpayby_api_complete',
    'access arguments' => array('access content'),
  );

  $items['webpayby/cancel'] = array(
    'title' => 'Payment is canceled',
    'page callback' => 'webpayby_api_cancel_page',
    'access arguments' => array('access content'),
  );

  return $items;
}

/**
 * Settings form.
 */
function webpayby_api_settings_form($form, &$form_state) {
  $form['webpayby_api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#required' => TRUE,
    '#default_value' => variable_get('webpayby_api_username'),
  );

  $form['webpayby_api_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#required' => TRUE,
    '#default_value' => variable_get('webpayby_api_password'),
  );
  
  $form['webpayby_api_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret key'),
    '#required' => TRUE,
    '#default_value' => variable_get('webpayby_api_secret_key'),
  );
  
  $form['webpayby_api_storeid'] = array(
    '#type' => 'textfield',
    '#title' => t('Store ID'),
    '#required' => TRUE,
    '#default_value' => variable_get('webpayby_api_storeid'),
  );
  
  $form['webpayby_api_storename'] = array(
    '#type' => 'textfield',
    '#title' => t('Store name'),
    '#maxlength' => 64,
    '#required' => TRUE,
    '#default_value' => variable_get('webpayby_api_storename'),
  );

  $form['webpayby_api_test_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Test mode'),
    '#default_value' => variable_get('webpayby_api_test_mode', TRUE),
  );
  
  return system_settings_form($form);
}

/**
 * Payment form.
 */
function webpayby_api_payment_form($form, &$form_state, $params) {
  $form['#action'] = variable_get('webpayby_api_test_mode', TRUE)
    ? 'https://secure.sandbox.webpay.by:8843'
    : 'https://payment.webpay.by';
  $form['#pre_render'][] = 'webpayby_api_payment_form_pre_render';

  $form['*scart'] = array(
    '#type' => 'hidden',
  );

  $form['wsb_storeid'] = array(
    '#type' => 'hidden',
    '#default_value' => variable_get('webpayby_api_storeid'),
  );

  $form['wsb_store'] = array(
    '#type' => 'hidden',
    '#default_value' => variable_get('webpayby_api_storename'),
  );

  $form['wsb_version'] = array(
    '#type' => 'hidden',
    '#default_value' => 2,
  );

  $form['wsb_seed'] = array(
    '#type' => 'hidden',
    '#default_value' => REQUEST_TIME,
  );

  $form['wsb_return_url'] = array(
    '#type' => 'hidden',
    '#default_value' => url('webpayby/complete', array('absolute' => TRUE)),
  );

  $form['wsb_cancel_return_url'] = array(
    '#type' => 'hidden',
    '#default_value' => url('webpayby/cancel', array('absolute' => TRUE)),
  );

  $form['wsb_notify_url'] = array(
    '#type' => 'hidden',
    '#default_value' => url('webpayby/notify', array('absolute' => TRUE)),
  );

  $form['wsb_test'] = array(
    '#type' => 'hidden',
    '#default_value' => (int)variable_get('webpayby_api_test_mode', TRUE),
  );

  foreach ($params as $key => $value) {
    $form['wsb_' . $key] = array(
      '#type' => 'hidden',
      '#default_value' => $value,
    );
  }

  $form['wsb_signature'] = array(
    '#type' => 'hidden',
    '#default_value' => sha1(
      $form['wsb_seed']['#default_value'] .
      $form['wsb_storeid']['#default_value'] .
      $form['wsb_order_num']['#default_value'] .
      $form['wsb_test']['#default_value'] .
      $form['wsb_currency_id']['#default_value'] .
      $form['wsb_total']['#default_value'] .
      variable_get('webpayby_api_secret_key')
    ),
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Pay'),
  );

  return $form;
}

/**
 * Form pre render callback.
 */
function webpayby_api_payment_form_pre_render($form) {
  unset($form['form_token']);
  unset($form['form_build_id']);
  unset($form['form_id']);
  unset($form['submit']['#name']);
  return $form;
}

/**
 * Notify menu callback.
 */
function webpayby_api_notify() {
  if (!webpayby_api_validate_notify_signature($_POST)) {
    return t('Failed validate transaction signature.');
  }

  $transaction = webpayby_api_get_transaction($_POST['transaction_id']);

  if (!webpayby_api_validate_transaction_signature($transaction)) {
    return t('Failed validate transaction signature.');
  }

  $payment_type = (string)$transaction->fields->payment_type;

  if ($_POST['site_order_id'] != (string)$transaction->fields->order_num) {
    return t('Order numbers do not match.');
  }
  if ($payment_type != 1 && $payment_type != 4) {
    return t('Payment transaction not complete.');
  }

  module_invoke_all('webpayby_api_notify', $_POST['site_order_id'], $transaction);
}

/**
 * Complete menu callback.
 */
function webpayby_api_complete() {
  if (empty($_GET['wsb_tid']) || empty($_GET['wsb_order_num'])) {
    return t('Not found transaction id or order number.');
  }

  $transaction = webpayby_api_get_transaction($_GET['wsb_tid']);

  if (!webpayby_api_validate_transaction_signature($transaction)) {
    return t('Failed validate transaction signature.');
  }

  $order_num = (string)$transaction->fields->order_num;
  $payment_type = (string)$transaction->fields->payment_type;

  if ($order_num != $_GET['wsb_order_num']) {
    return t('Order numbers do not match.');
  }
  if ($payment_type != 1 && $payment_type != 4) {
    return t('Payment transaction not complete.');
  }

  $result = module_invoke_all('webpayby_api_complete', $order_num, $transaction);
  $result = array_diff($result, array(''));

  return $result ? implode("\n", $result) : t('Payment complete');
}

/**
 * Cancel menu callback.
 */
function webpayby_api_cancel_page() {
  return t('Unknown error');
}

/**
 * Return transaction info in simplexml object.
 */
function webpayby_api_get_transaction($transaction_id) {
  $postdata = '*API=&API_XML_REQUEST=<?xml version="1.0" encoding="ISO-8859-1" ?>
    <wsb_api_request>
      <command>get_transaction</command>
      <authorization>
        <username>' . variable_get('webpayby_api_username') . '</username>
        <password>' . md5(variable_get('webpayby_api_password')) . '</password>
      </authorization>
      <fields>
        <transaction_id>' . $transaction_id . '</transaction_id>
      </fields>
    </wsb_api_request>
  ';
  $url = variable_get('webpayby_api_test_mode', TRUE)
    ? 'https://sandbox.webpay.by'
    : 'https://billing.webpay.by';

  $result = drupal_http_request($url, array(
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
    'method' => 'POST',
    'data' => $postdata,
  ));

  if ($result->code == 200) {
    $transaction = simplexml_load_string($result->data);

    if ((string)$transaction->status == 'failed') {
      watchdog('webpayby_api', 'Failed to get transaction. Result dump: ' . _webpayby_api_var_dump($result));
    }
    else {
      return $transaction;
    }
  }
  else {
    watchdog('webpayby_api', 'Failed to get transaction. Result dump: ' . _webpayby_api_var_dump($result));
  }
}

/**
 * Return TRUE if notify signature is valid.
 */
function webpayby_api_validate_notify_signature($data) {
  if (!$data) {
    return FALSE;
  }

  $calculated_signature = md5(
    $data['batch_timestamp'] .
    $data['currency_id'] .
    $data['amount'] .
    $data['payment_method'] .
    $data['order_id'] .
    $data['site_order_id'] .
    $data['transaction_id'] .
    $data['payment_type'] .
    $data['rrn'] .
    variable_get('webpayby_api_secret_key')
  );

  return $data['wsb_signature'] === $calculated_signature;
}

/**
 * Return TRUE if transaction signature is valid.
 */
function webpayby_api_validate_transaction_signature($transaction) {
  if (!$transaction) {
    return FALSE;
  }

  $calculated_signarure = md5(
    $transaction->fields->transaction_id .
    $transaction->fields->batch_timestamp .
    $transaction->fields->currency_id .
    $transaction->fields->amount .
    $transaction->fields->payment_method .
    $transaction->fields->payment_type .
    $transaction->fields->order_id .
    $transaction->fields->rrn .
    variable_get('webpayby_api_secret_key')
  );

   return (string)$transaction->fields->wsb_signature === $calculated_signarure;
}

/**
 * Return variable dump in string.
 */
function _webpayby_api_var_dump($var) {
  return '<pre>' . check_plain(print_r($var, TRUE)) . '</pre>';
}
